# CHD4630 - Methods of Studying Families and Children
## Andrew Benesh 

## Description

CHD4630, Methods of Studying Families and Children, is a research methods course for advanced undergraduates studying Family and Child Sciences (FCS).
This course covers many topics traditionally addressed in social science research methods courses, but tailors content to applications in FCS.
Because FCS majors come from diverse backgrounds and have widely varying levels of prior experience with research and statistics, this course attempts to cover both basic principles and applications and explore more complex research strategies that are traditionally reserved for beginning graduate students.
As an aid to student learning, course materials are hosted online at [benesh.info/CHD4630](http://www.benesh.info/CHD4630).

## Contributors

The contents of this repository were developed by Andrew Benesh.

