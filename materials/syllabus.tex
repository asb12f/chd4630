% Document settings
\documentclass[11pt]{article}
\usepackage[margin=0.6in]{geometry}
\usepackage[pdftex]{graphicx}
\usepackage{multirow}
\usepackage{setspace}
\usepackage{hyperref}
\hypersetup{colorlinks = true, linkcolor = blue, urlcolor = blue, citecolor = blue}
\usepackage{apacite}
\usepackage{bibentry}
\bibliographystyle{apacite}

\setlength\parindent{0pt}

\begin{document}

% Course information
\begin{tabular}{ l l }
  \multirow{3}{*}{\includegraphics[height=1.25in,width=1.25in]{logo.png}} 
  & \LARGE Methods of Studying Children and Families \\\\
  & \LARGE CHD 4630-0001 Summer 2015\\\\
\end{tabular}
\vspace{10mm}

% Professor information
\begin{tabular}{ l l l}
\\
  & \large Instructor: & Andrew Benesh, MFT \\
  & \large Email: & \href{mailto:asb12f@my.fsu.edu}{asb12f@my.fsu.edu} \\
  & \large Office: & Sandels 211 \\
  & \large Virtual Office Hours: & Monday 3:00--5:00PM \\
  & \large & or by appointment \\
  & \large Skype: & andrew.benesh1\\
  & \large External Course Website: & \href{http://www.benesh.info/CHD4630}{benesh.info/CHD4630} \\
\end{tabular}
\vspace{3mm}

% Course details
\section*{Course Description}
 The goal of this course is to teach students to understand and apply the basic research concepts, methods, and designs used within the study of children and families.
Students will learn to think critically in approaching research problems and assessing published research, how to apply research concepts and skills to research problems, and develop research proposals to answer research questions about children and families.  
This course will be helpful to prepare students for future family and child sciences coursework, graduate education, and careers that incorporate or rely on research.  

\subsection*{Course Objectives} 
\begin{enumerate}
\setlength\itemsep{-0.6em}
\item Identify major research problems and themes involving children and families.
\item Demonstrate the ability to read, interpret, utilize, and critique research on children and families in popular press and academic journals. 
\item Formulate good research questions and practice strategies to answer questions through literature reviews and research studies.
\item Describe and apply different methods of data collection and identify the advantages and disadvantages of each method. 
\item Define research designs and develop a research proposal in a systematic and organized fashion.
\item Report results of research pertaining to FCS using APA style.  
\item Distinguish high quality from poor quality research. 
\end{enumerate} 

\subsection*{Textbooks} 
You will use three books for this course.
All three texts are \textbf{required} for the course.
Students should obtain all three texts by the second week of class. 
In recognition of the growing cost of textbooks, this course will use two open source textbooks that are available online at \textbf{no cost}. 
Additional readings will be provided throughout the course.\\

\begin{tabular}{l l}
\textbf{Title:} & Publication Manual of the American Psychological Association, 6th Edition \\
\textbf{Author:}& American Psychological Association \\
\textbf{Year:} &2009 (or later printings) \\
\textbf{Publisher:}& American Psychological Association \\
\textbf{ISBN13:} & 978-1433805615 \\
\textbf{Get it Here:} & \href{http://www.amazon.com/Publication-American-Psychological-Association-Edition/dp/1433805618}{Amazon}; \href{http://www.barnesandnoble.com/w/publication-manual-of-the-american-psychological-association-american-psychological-association/1100212057?ean=9781433805615}{Barnes \& Noble}; \href{http://www.apastyle.org/manual/}{APA }\\
& \\
\textbf{Title:} & Social Science Research: Principles, Methods, and Practices \\
\textbf{Author:}&  Anol Bhattacherjee \\
\textbf{Year:} &2012 \\
\textbf{Publisher:}& University of South Florida \\
\textbf{ISBN13:} &978-1475146127 \\
\textbf{Get it Here:}& \href{http://scholarcommons.usf.edu/oa\_textbooks/3/}{USF Scholars Commons}; \href{http://open.umn.edu/opentextbooks/BookDetail.aspx?bookId=79}{Open Textbook Library} \\
& \\
\textbf{Title:} & Research Methods in Psychology \\
\textbf{Author:}&  Paul Price \\
\textbf{Year:} & 2012 \\
\textbf{Publisher:}& California State University \\
\textbf{ISBN13:} & 978-1-4533236-0-1 \\
\textbf{Get it Here:}& \href{http://open.umn.edu/opentextbooks/BookDetail.aspx?bookId=75}{Open Textbook Library}\\ 
\textit{Note:} & \it{This text will be primarily used to supplement the Bhatterchjee text. Readings for this text} \\
&  \it{listed in the schedule are not required, but are instead meant as a resource to supplement or} \\
& \it{clarify the material.}\\
\end{tabular}

\section*{Process \& Content}

\subsection*{Course Content}
The instructor of this class is responsible for organizing information related to the course content and for developing learning experiences.
This class emphasizes a variety of concepts, principles, and applications to research.  
Students should read \textbf{before} class to ensure that they are adequately prepared.

\subsection*{Course Activities}
Learning research methods requires students to develop new vocabulary, critically evaluate research problems, creatively generate research strategies, and write using technical language and formats.
As a result, research methods courses tend to differ from more topic oriented courses within FCS.
Students should approach the course in a manner similar to how they would approach a math or foreign language course.

\subsection*{Attendance \& Scheduling} 
There are strict deadlines for assignments and test taking, but you have flexibility and responsibility for setting an individualized schedule to read assignments, and prepare for tests, and complete assignments in order to meet these deadlines. 
A tentative course outline with due dates is provided at the end of this syllabus.

\subsection*{Announcements \& Communication}
It is important to use appropriate online forms of communication to stay up to date, and students should frequently view the course schedule and attend to deadlines.
There will be announcements in Blackboard \& emails as a way of letting you know about something that either has changed or needs clarification.  It is your responsibility to frequently read announcements and emails for the course (suggested at least THREE TIMES each week).

When communicating online, it is essential to follow outstanding levels of etiquette/civil discourse.  
Typed words have a lasting impact. 
Placing ideas about issues in a positive framework will more likely lead to learning new ideas, collegiality, and enlightening interactions with peers and instructor.  
Be respectful of the thoughts and experiences of your instructor and the class as a whole.

\paragraph*{If you need to contact the instructor via email, it is important that you use your official school email address, and include CHD4630 in the title of your email. Other emails may be sorted as spam, or receive delayed responses}

\section*{Course Policies}

\section*{Assignments}
The following is a brief overview of each assignment. 
See the rubric on Blackboard for detailed instructions on assignment expectations and grading.
Unless otherwise specified, all assignments are due by 11:59PM on the Friday of the week they are due.
\subsection*{Individual Assignments}
\paragraph*{Interview Paper (25pts)}
For this assignment, students will interview a professional in family or child sciences about how research is used in their professional work.
Students are encouraged to choose professionals in careers or work areas that are personally interesting or relevant.

\paragraph*{Ethics Assignment (25pts)}
For this assignment, students will complete FSU's ethics training, and apply your knowledge of ethics to a recent case of ethics violations in research.

\paragraph*{Article Review (25pts each)}
Students will read and critically evaluate an articles provided by the instructor. 

\paragraph{Content Quizzes (4 Pts Each)}
To track student learning throughout the course, five short quizzes will be administered during the semester. 

\subsection*{Group Research Project}
Because research is a collaborative activity, students will work together in a group to carry out a small scale survey research project over the course of the semester.

\paragraph*{Group Project Proposal (30pts)}
In groups of up to 5, students will assemble a project proposal which will include a statement of the research question, a literature review on the research topic, sampling and measurement plans, and plans for basic data analysis. 

\paragraph*{Group Project Paper(50pts)}
Students will present the results of their group project in a formal research report.

\subsection*{First Day Attendance}
Students who are not present the ``first day'' of class must be dropped according to university policy. 

\subsection*{Grading}

\paragraph*{Final Grades} Grades will be based on a point system comprised of a total of 400 points as indicated below (go by point value already inclusive of rounding up).  
A plus/minus system will be in effect although there are no grades of A+, F+ or F-.  
No assignments will be accepted after the last day of class.  
There will be no rewrites of assignments in this course.  
There will be no extra credit in this class. 
\textbf{Rounding allowed for this class already is included for point totals below so do not ask for additional rounding at semester end}.  \\

\textbf {Points Distribution:} \\\\
\hspace*{10mm}
\begin{tabular}{ l | l }
Interview Paper & 25pts\\
Article Review (2) & 50pts (25pts each) \\
Ethics Assignment & 25pts \\
Group Project Proposal & 30pts \\
Group Project Paper & 50pts\\
Quizzes (5) & 20pts \\
Exams (2)  & 200pts (100pts each) \\\hline
Total Points & 400pts\\
\end{tabular} \\\\

\textbf {Letter Grade Distribution:} \\\\
\hspace*{10mm}
\begin{tabular}{ l l l | ll  l }
Percent & Points & Grade & Percent & Points & Grade \\ \hline
\textgreater= 92.00 & (\textgreater= 368)& A & 72 - 77\% &(288-311)& C \\
90 - 91\% &(360-367)& A-  & 70 - 71\% &(280-287)& C- \\
88 - 89\% &(352-359)& B+  & 68- 69\% &(272-279)& D+ \\
82 - 87\% &(328-351)& B  & 62- 67\% &(248-271)& D \\
80 - 81\% &(320-327)& B-  & 60 - 61\% &(240-247)& D- \\
78 - 79\% &(312-319)& C+  & \textless= 59\% & (\textless= 239) & F \\
\end{tabular} \\

\paragraph*{Grading Questions} Exams and assignments will be graded quickly, but grading a large number of assignments takes time.
It is your responsibility to be aware of your grades consistently throughout the course, in order to prevent any surprises at the end of the semester.
If you desire clarification of your score on an exam or assignment, you have 1 week after scores are posted to contact your instructor to make an appointment.
You are encouraged to seek clarification but it is assumed you understand your score if you ask no questions within one week of any test or assignment.  
Students should check for grades posted in Blackboard regularly.

\paragraph*{Missed Exams}
Make-up exams will be given only in cases of ``FSU excused absences''.
It is the student’s responsibility to notify the instructor within 24 hours of the missed exam or assignment and to make it up within one week of the due date. 
If you do not have verifiable documentation for your absence you will not be allowed to make it up. 
Absences are excused only when verifiable:  DOCUMENTATION of illness (from the health center or medical doctor), loss of family member (from obituary), FSU athletics involvement (from coach), or religious holiday presented to the instructor.  
Consideration will also be given to students whose dependent children experience serious illness, with proper medical documentation.  
Please let the instructor know as soon as possible if chronic health problems and/or a personal emergency threaten to interfere with your regular attendance and required work for this class; the instructor will work to facilitate the best resolution in extreme cases.  
Only the days indicated on the documentation will be accepted as verification of an excused absence. 
Activities such as job interviews, weddings, work schedule, etc., are NOT considered excused absences.

\paragraph*{Missed Assignments}
To receive full credit for your assignments, you must turn them in on or before their due date.
Assignments may be submitted after their due date, but will have two points deducted for every day they are late, unless the student provides documentation of an excused absence.

\paragraph*{Missed Quizzes}
To receive full credit for in class quizzes, you must be present in class and turn in your quiz before the end of that class period. 
Students who are absent will be allowed to make up missed quizzes only if they provide documentation of an excused absence.

\paragraph*{Exam and Quiz Tardiness}
Students will be allowed to take any quiz or exam they arrive tardy to only if no students have turned in their test or quiz.
Any student who arrives late for a test or quiz after the first test or quiz has been turned in will receive a grade of zero unless they provide documentation of an excusable absence.

\subsection*{Class Participation and Decorum}
All class members have a responsibility to keep current with reading and all assignments. 
This participation is essential for your learning. 
Disrespectful behavior toward other students, the course instructor, or anyone associated with the class is unacceptable and can have consequences according to the FSU honor code.  

\subsection*{University Attendance Policy}
Excused absences include documented illness, deaths in the family and other documented crises, call to active military duty or jury duty, religious holy days, and official University activities.  
These absences will be accommodated in a way that does not arbitrarily penalize students who have a valid excuse.  
Consideration will also be given to students whose dependent children experience serious illness. 

\subsection*{Academic Honor Policy}
The Florida State University Academic Honor Policy outlines the University’s expectations for the integrity of students’ academic work, the procedures for resolving alleged violations of those expectations, and the rights and responsibilities of students and faculty members throughout the process.  
Students are responsible for reading the Academic Honor Policy and for living up to their pledge to “. . . be honest and truthful and . . . [to] strive for personal and institutional integrity at Florida State University.”  (Florida State University Academic Honor Policy, found at \href{http://fds.fsu.edu/Academics/Academic-Honor-Policy}{http://fds.fsu.edu/Academics/Academic-Honor-Policy}.)

\subsection*{Americans with Disabilities Act}
Students with disabilities needing academic accommodation should: (1) register with and provide documentation to the Student Disability Resource Center; and (2) bring a letter to the instructor indicating the need for accommodation and what type.  
This should be done during the first week of class.  This syllabus and other class materials are available in alternative format upon request.
 For more information about services available to FSU students with disabilities, contact the: \\

Student Disability Resource Center \\
874 Traditions Way\\
108 Student Services Building\\
Florida State University\\
Tallahassee, FL 32306-4167\\
(850) 644-9566 (voice)\\
(850) 644-8504 (TDD)\\
\href{mailto:sdrc@admin.fsu.edu}{sdrc@admin.fsu.edu} \\
\href{http://www.disabilitycenter.fsu.edu/}{http://www.disabilitycenter.fsu.edu/}\\

\subsection*{Syllabus Change Policy}
Except for changes that substantially affect implementation of the evaluation (grading) statement, this syllabus is a guide for the course and is subject to change with advance notice.  

\subsection*{Free Tutoring from FSU}
On-campus tutoring and writing assistance is available for many courses at Florida State University.  
For more information, visit the Academic Center for Excellence (ACE) Tutoring Services’ comprehensive list of on-campus tutoring options at \href{http://ace.fsu.edu/tutoring}{http://ace.fsu.edu/tutoring} or contact \href{mailto:tutor@fsu.edu}{tutor@fsu.edu}.  
High-quality tutoring is available by appointment and on a walk-in basis.
 These services are offered by tutors trained to encourage the highest level of individual academic success while upholding personal academic integrity.
 
\section*{Advice from Former Students}
\begin{itemize}
\item I would tell them to \textbf{NOT} fall behind. Keep up with the readings and lectures. It is not a terribly hard course but it is a lot of material and if you fall behind it can be overwhelming.
\item To read ahead and set out specific times to complete tasks
\item Take notes from the textbook. Email if you are stuck or need clarification. 
\item You \textbf{HAVE} to put in the hours and work to get a good grade. 
\item Read material as early as possible so you don't fall behind
\item Read every page of the book that is assigned. Take extremely detailed notes. Visit the office hours. Listen to each lecture, also take detailed notes.
\item You should make sure you are able to apply the material that you are learning to real life article and research papers to make sure you understand the material 100\%
\item Begin thinking of concepts in terms of examples as soon as you start reading, because this will help in fully comprehending the course material.
\item Study beforehand. The information is based off of the textbook and lectures. 
\item Focus on examples and concepts not definitions and focus on the lectures because the book makes things more confusing than they need to be sometimes. 
\item Do not slack and wait till the last minute to do the assignments, stay on top of reading and work with a partner from class when doing papers, helps you to make sure your staying on the right track
\item Keep up with the work. It is a lot of information take in. You must both learn the information and apply it.
\item Keep up with the readings; do not wait until the last day to start research papers; and practice searching for peer-reviewed articles (it can be frustrating at times when you can't find the exact article you are looking for; so key terms are important to use when searching for articles).
\item  \textbf{GO TO THE OFFICE HOURS}. Come prepared with questions and demand answers that will help you.

\subsection*{My Advice}
As my past students have said, keeping up with the readings and assignments is critical to your success in this course.
Focus on being able to apply the ideas we talk about in class.
Never be afraid to ask questions -- in office hours, or by email -- and if the answer I give you isn't clear or helpful, \textbf{don't be afraid to ask again} until you feel like you have a good understanding of the concepts and material! 

\textbf{Take advantage of feedback}.
Due to the class size and the pace of summer semesters, I only provide basic feedback on graded assignments. 
However, I'm always willing to provide more detailed feedback on completed assignments upon request. 
I'm also always willing to provide feedback on drafts of assignments before you turn them in, provided you submit the drafts to me in a timely manner. 
\end{itemize} 

%This is a reference list of all articles and books used or referenced in the course and assignments.
\nocite{tonge2014randomised}
\nocite{kramer2014experimental}
\nocite{schechter2014using}
\nocite{simon2014corporate}
\nocite{richardson2014siblings}
\nocite{american2009publication}
\nocite{bhattacherjee2012social}
\nocite{price2012Research}
\nocite{welch2006outcomes}
\nocite{myeroff1999comparative}
\nocite{pignotti2007holding}
\nocite{mercer2005coercive}
\nocite{vromans2011narrative}
\nocite{roberts2000narrative}
\nocite{lopes2014narrative}
\nocite{rothschild2000narrative}
\nocite{buckner1983dramatic}
\nocite{lewis1983scared}
\nocite{petrosino2003scared}
\nocite{petrosino2000well}
\nocite{windell2005application}
\nocite{feinstein2008energy}
\nocite{rosen2001echo}
\nocite{pignotti2005callahan}
\nocite{callahan2001impact}
\nocite{connolly2010treatment}

\bibliography{asstRefs}

\newpage
\begin{table}[h!]
\caption{Tentative Schedule}
\begin{tabular}{ | c | c | c | }
\hline
\textbf{Week} & \textbf{Dates} & \textbf{Content} \\
\hline
Week 1 & May 11 -- 15 & \begin{minipage}{.6\textwidth}
\begin{itemize} \itemsep-0.4em
	\vspace{1mm}
	\item Course Overview
	\item Topic: What is Research, and Why is it Important?
	\item Reading: Bhat Ch 1; Price Ch 1
	\vspace{1mm}
\end{itemize}
\end{minipage} \\
\hline
Week 2 & May 17 -- 22 & \begin{minipage}{.6\textwidth}
\begin{itemize} \itemsep-0.4em
	\vspace{1mm}
	\item Topic: Research Questions, Theories, \& Hypotheses
	\item Reading: Bhat Ch 2 \& 3; Price Ch 2.1, 2.2, \& 2.4
	\item Due: Interview Paper
	\vspace{1mm}
\end{itemize}
\end{minipage} \\
\hline
Week 3 & May 25 -- May 29 & \begin{minipage}{.6\textwidth}
\begin{itemize} \itemsep-0.4em
	\vspace{1mm}
	\item Topic: Ethics in Research 
	\item Reading: 	Bhat Ch 16; Price Ch 3
	\item Topic: Literature Reviews and APA Format
	\item Reading: APA Ch 2 \& 3; Price Ch 2.3 \& Ch 11
	\vspace{1mm}
\end{itemize}
\end{minipage} \\
\hline
Week 4 & June 1 -- June 5 & \begin{minipage}{.6\textwidth}
\begin{itemize} \itemsep-0.4em
	\vspace{1mm}
	\item Topic: Sampling
	\item Reading: Bhat Ch 8; Price Ch 9.3
	\item Due: Ethics Paper
	\vspace{1mm}
\end{itemize}
\end{minipage} \\
\hline
Week 5 & June 8 -- June 12 &\begin{minipage}{.6\textwidth}
\begin{itemize} \itemsep-0.4em
	\vspace{1mm}
	\item Topic: Conceptualization and Measurement
	\item Reading: Bhat Ch 6 \& 7; Price Ch 5
	\vspace{1mm}
\end{itemize}
\end{minipage} \\
\hline
Week 6 & June 15 -- June 19 & \begin{minipage}{.6\textwidth}
\begin{itemize} \itemsep-0.4em
	\vspace{1mm}
	\item Topic: Research Design: An Overview
	\item Reading: Bhat Ch 4 \& 5; Price Ch 5
	\item Due: Group Project Proposal
	\vspace{1mm}
\end{itemize}
\end{minipage} \\
\hline
Week 7 & June 22 -- 26 & \begin{minipage}{.6\textwidth}
\begin{itemize} \itemsep-0.4em
	\vspace{1mm}
	\item Exam 1
	\vspace{1mm}
\end{itemize}
\end{minipage} \\
\hline
Week 8 & June 29 -- July 3 &\begin{minipage}{.6\textwidth}
\begin{itemize} \itemsep-0.4em
	\vspace{1mm}
	\item Topic: Research Design \& Statistical Analysis
	\item Reading: Bhatt Ch 14 \& 15; Price Ch 12 \& 13
	\item Due: Article Review \#1
	\vspace{1mm}
\end{itemize}
\end{minipage} \\
\hline
Week 9 & July 6 -- July 10 & \begin{minipage}{.6\textwidth}
\begin{itemize} \itemsep-0.4em
	\vspace{1mm}
	\item Topic: Survey Research
	\item Reading: Bhat Ch 9; Price Ch 9
	\vspace{1mm}
\end{itemize}
\end{minipage} \\
\hline
Week 10 & July 13 -- 17 & \begin{minipage}{.6\textwidth}
\begin{itemize} \itemsep-0.4em
	\vspace{1mm}
	\item Topic: Experimental Research
	\item Reading: Bhat Ch 10; Price Ch 6 \& 7.1, 7.2, \& 7.3
	\item Due: Article Review \#2
	\vspace{1mm}
\end{itemize}
\end{minipage} \\
\hline
Week 11 & July 20--24 & \begin{minipage}{.6\textwidth}
\begin{itemize} \itemsep-0.4em
	\vspace{1mm}
	\item Topic: Case Research
	\item Reading: Bhat Ch 11; Price Ch 10
	\vspace{1mm}
\end{itemize}
\end{minipage} \\
\hline
Week 12 & July 27 -- 31 & \begin{minipage}{.6\textwidth}
\begin{itemize} \itemsep-0.4em
	\vspace{1mm}
	\item Topic: Interpretive and Qualitative Research
	\item Reading: Bhat Ch 13; Price Ch 7.4 
	\vspace{1mm}
\end{itemize}
\end{minipage} \\
\hline
Week 13 & August 3 -- August 7 & \begin{minipage}{.6\textwidth}
\begin{itemize} \itemsep-0.4em
	\vspace{1mm}
	\item Exam 2
	\item Group Project Paper Due
	\vspace{1mm}
\end{itemize}
\end{minipage} \\
\hline
\end{tabular} 
\end{table}

\end{document}




