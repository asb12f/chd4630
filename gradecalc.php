<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>CHD4630 Grade Calculator</title>

	<!-- Google Tag Manager -->
		<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KL4WL7"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-KL4WL7');</script>
	<!-- End Google Tag Manager -->


<!-- This is a quick and dirty grade calculator; it's not terribly pretty --->

<?php
$interview = filter_input(INPUT_GET, 'interview', FILTER_SANITIZE_SPECIAL_CHARS);
$qual = filter_input(INPUT_GET, 'qual', FILTER_SANITIZE_SPECIAL_CHARS);
$quant = filter_input(INPUT_GET, 'quant', FILTER_SANITIZE_SPECIAL_CHARS);
$ethics = filter_input(INPUT_GET, 'ethics', FILTER_SANITIZE_SPECIAL_CHARS);
$ethics2 = filter_input(INPUT_GET, 'ethics2', FILTER_SANITIZE_SPECIAL_CHARS);
$proposal = filter_input(INPUT_GET, 'proposal', FILTER_SANITIZE_SPECIAL_CHARS);
$data = filter_input(INPUT_GET, 'data', FILTER_SANITIZE_SPECIAL_CHARS);
$paper = filter_input(INPUT_GET, 'paper', FILTER_SANITIZE_SPECIAL_CHARS);
$exam1 = filter_input(INPUT_GET, 'exam1', FILTER_SANITIZE_SPECIAL_CHARS);
$exam2 = filter_input(INPUT_GET, 'exam2', FILTER_SANITIZE_SPECIAL_CHARS);
$quiz1 = filter_input(INPUT_GET, 'quiz1', FILTER_SANITIZE_SPECIAL_CHARS);
$quiz2 = filter_input(INPUT_GET, 'quiz2', FILTER_SANITIZE_SPECIAL_CHARS);
$quiz3 = filter_input(INPUT_GET, 'quiz3', FILTER_SANITIZE_SPECIAL_CHARS);
$quiz4 = filter_input(INPUT_GET, 'quiz4', FILTER_SANITIZE_SPECIAL_CHARS);
$quiz5 = filter_input(INPUT_GET, 'quiz5', FILTER_SANITIZE_SPECIAL_CHARS);
$extra = filter_input(INPUT_GET, 'extra', FILTER_SANITIZE_SPECIAL_CHARS);

$total = filter_input(INPUT_GET, 'total', FILTER_SANITIZE_SPECIAL_CHARS);

if (isset($total)){
	$sum = $total;
} else {
	$sum = $interview + $qual + $quant + $ethics + $ethics2 + $proposal + $paper + $data + $exam1 + $exam2 + $quiz1 + $quiz2 + $quiz3 + $quiz4 + $quiz5 + $extra + 1;
	$total = $sum;
}

if ($sum > 406){
	$grade = "I'm pretty sure that's impossible.";
} elseif ($sum >= 368) {
	$grade = "<span style='color: green;'> A </span>";
} elseif ($sum >= 360) {
	$grade = "A-";
} elseif ($sum >= 352) {
	$grade = "B+";
} elseif ($sum >= 328) {
	$grade = "B";
} elseif ($sum >= 320) {
	$grade = "B-";
} elseif ($sum >= 312) {
	$grade = "C+";
} elseif ($sum >= 288) {
	$grade = "C";
} elseif ($sum >= 280) {
	$grade = "C-";
} elseif ($sum >= 272) {
	$grade = "D+";
} elseif ($sum >= 248) {
	$grade = "D";
} elseif ($sum >= 240) {
	$grade = "D-";
} else {
	$grade = "F";
}
?>

<style>
body {
	padding: 1em;
}

</style>

</head>
<body>
<div class="container-fluid">
<div class="row">
<div class="page-header col-md-8 col-md-offset-2">
  <h1>Grade Calculator <small>for CHD4630 Summer 2015</small></h1>
</div>
</div>
<div class="row">
<form action="gradecalc.php" method="GET" class=" col-md-8 col-md-offset-2" >
	<div class="row">
	
	<legend>Calculate by Assignment</legend>
	<div class="form-group form-inline">
		<label class="control-label col-md-9" for="interview">1st Day Attendance (1pts)</label>
		<input class="form-control col-md-3" type="number" min="0" max="25" name="interview" value="1">
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-" for="interview">Interview Assignment (<?php echo $interview?>/25pts = <?php echo round($interview /25 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" min="0" max="25" name="interview" value="<?php echo $interview?>">
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9" for="qual">Qualitative Assignment (<?php echo $qual?>/25pts = <?php echo round($qual /25 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number"  min="0" max="25" name="qual" value="<?php echo $qual?>">	
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="quant">Quantitative Assignment (<?php echo $quant?>/25pts = <?php echo round($quant /25 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="quant" min="0" max="25" value="<?php echo $quant?>">
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="ethics">Ethics Assignment (<?php echo $ethics?>/15pts = <?php echo round($ethics /15 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="ethics" min="0" max="15" value="<?php echo $ethics?>">
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="ethics2">Human Subjects Training (<?php echo $ethics2?>/10pts = <?php echo round($ethics2 /10 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="ethics2" min="0" max="10" value="<?php echo $ethics2?>">
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="proposal">Group Proposal Assignment (<?php echo $proposal?>/30pts = <?php echo round($proposal /30 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="proposal"  min="0" max="30"value="<?php echo $proposal?>">
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="paper">Group Paper Assignment (<?php echo $paper?>/50pts = <?php echo round($paper /50 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="paper" min="0" max="50" value="<?php echo $paper?>">
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="data">Group Paper Data (<?php echo $data?>/4pts = <?php echo round($data /4 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="data" min="0" max="50" value="<?php echo $data?>">
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="exam1">Exam 1 (<?php echo $exam1?>/100pts = <?php echo round($exam1 /100 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="exam1"  min="0" max="100" value="<?php echo $exam1?>">
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="exam2">Exam 2 (<?php echo $exam2?>/100pts = <?php echo round($exam2 /100 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="exam2" min="0" max="105" value="<?php echo $exam2?>">
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="quiz1">Quiz1 Assignment (<?php echo $quiz1?>/4pts = <?php echo round($quiz1 /4 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="quiz1"  step="0.01" min="0" max="4" value="<?php echo $quiz1?>">
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="quiz2">Quiz2 Assignment (<?php echo $quiz2?>/4pts = <?php echo round($quiz2 /4 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="quiz2" step="0.01" min="0" max="4" value="<?php echo $quiz2?>">
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="quiz3">Quiz3 Assignment (<?php echo $quiz3?>/4pts = <?php echo round($quiz3 /4 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="quiz3"  step="0.01" min="0" max="4" value="<?php echo $quiz3?>">
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="quiz4">Quiz4 Assignment (<?php echo $quiz4?>/4pts = <?php echo round($quiz4 /4 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="quiz4" step="0.01" min="0" max="4" value="<?php echo $quiz4?>">
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="quiz5">Quiz5 Assignment  (<?php echo $quiz5?>/4pts = <?php echo round($quiz5 /4 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="quiz5" step="0.01" min="0" max="4" value="<?php echo $quiz5?>">
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="extra">Extra Credit Assignment (<?php echo $extra?>/5pts = <?php echo round($extra /5 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="extra"  min="0" max="5" value="<?php echo $extra?>">
	</div>
	</div>
	<div class="row">
	<div class="form-group form-inline">
	<button type="submit" class="btn btn-success ">Calculate!</button>
	</div>
	</div>

</form>
</div>

<div class="row">
<form action="gradecalc.php" method="GET" class=" col-md-8 col-md-offset-2" >
	<div class="row">
	<legend>Calculate by Total Points</legend>
	<div class="form-group form-inline">
		<label class="control-label col-md-9" for="total">Total Points</label>
		<input class="form-control col-md-3" type="number" step="0.01" name="total" value="<?php echo $total?>">
	</div>
	</div>
	<div class="row">
	<div class="form-group form-inline">
	<button type="submit" class="btn btn-success ">Calculate!</button>
	</div>
	</div>

</form>
</div>

<div class="row">
	<div class="col-md-8 col-md-offset-2 centered">
	<hr>
	<dl class="dl-horizontal lead">
	<dt>Current Points: </dt><dd><?php echo $sum ?> of 400 Possible</dd>
	<dt>Percentage: </dt><dd><?php echo ($sum / 400)*100 ?>%</dd>
	<dt>Grade: </dt><dd><?php echo $grade ?></dd>
	</dl>
	<h2>Questions? <a href="mailto:asb12f@my.fsu.edu">Email me!</a></h2>
	</div>
</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</html>